from django.db import models

class Question(models.Model):
    """ Модель вопроса для голосования """
    question_text = models.CharField(max_length=255)
    pub_date = models.DateTimeField('publish date')


class Choise(models.Model):
    """ Модель хранения результов ответа на вопрос """
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    chose_text = models.CharField(max_length=255)
    voites = models.IntegerField(default=0)


