from django.shortcuts import HttpResponse

def index(request):
    """ Базовое представление начальной страницы. """
    return HttpResponse("Hello, world. Yue're at the polls index.")
